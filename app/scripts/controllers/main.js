'use strict';

/**
 * @ngdoc function
 * @name muestrasApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the muestrasApp
 */
angular.module('muestrasApp')
  .controller('MainCtrl', function ($scope, solicitudService, filterFilter) {

    //Controles navegacion
    var navListItems = $('ul.setup-panel li a'), allWells = $('.setup-content');

    allWells.hide();

    $(".titulo").affix({
      offset: {
        top: 30
      }
    });

    navListItems.click(function(e){
      e.preventDefault();
      var $target = $($(this).attr('href')), $item = $(this).closest('li');

      if (!$item.hasClass('disabled')) {
        navListItems.closest('li').removeClass('active');
        $item.addClass('active');
        allWells.hide();
        $target.show();
      }
    });

    $('ul.setup-panel li.active a').trigger('click');

    $('#activate-step-2').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-2"]').trigger('click');
    });

    $('#activate-step-3').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
    });

    $('#activate-step-4').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-4"]').trigger('click');
    });

    $('#activate-step-1').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-1"]').trigger('click');
    });

    $('#activate-step-2ant').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-2"]').trigger('click');
    });

    $('#activate-step-3ant').on('click', function(e) {
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
    });

    var fecha = new Date();

    var dia = fecha.getDate();
    var mes = (fecha.getMonth()+1);
    var hora = (fecha.getHours());
    var minutos = (fecha.getMinutes());

    if(dia< 10){
      dia = "0" + dia;
    }
    if(mes< 10){
      mes = "0" + mes;
    }
    if(hora< 10){
      hora = "0" + hora;
    }
    if(minutos< 10){
      minutos = "0" + minutos;
    }

    reset();

    //datos
    $scope.fechaSolicitud = dia+"/"+mes+"/"+fecha.getFullYear()+" "+hora+":"+minutos;
    $scope.valores = [
      {id: 1, nombre: "250µ"},
      {id: 2, nombre: "500µ"},
      {id: 3, nombre: "1mm"},
      {id: 4, nombre: "2mm"},
      {id: 5, nombre: "4mm"},
      {id: 6, nombre: "5mm"},
      {id: 7, nombre: "6,3mm"},
      {id: 8, nombre: "8mm"},
      {id: 9, nombre: "10mm"},
      {id: 10, nombre: "12mm"},
      {id: 11, nombre: "12,5mm"},
      {id: 12, nombre: "14mm"},
      {id: 13, nombre: "20mm"},
      {id: 14, nombre: "31,5mm"}
    ];

    //Materiales
    $scope.materiales1 = [
      { name: 'Plásticos', selected: false },
      { name: 'Papel/cartón', selected: false },
      { name: 'Madera', selected: false },
      { name: 'Caucho', selected: false },
      { name: 'Materia orgánica biodegradable', selected: false }
    ];
    $scope.materiales2 = [
      { name: 'Textil', selected: false },
      { name: 'Cerámica/piedras', selected: false },
      { name: 'Vidrio', selected: false },
      { name: 'Metales férricos', selected: false },
      { name: 'Metales no férricos', selected: false },
      { name: 'Otros', selected: false }
    ];

    $scope.$watch('materiales1 | filter:{selected:true}', function (nv) {
      $scope.selectionMateriales1 = nv.map(function (elem) {
        return elem.name;
      });
    }, true);
    $scope.$watch('materiales2 | filter:{selected:true}', function (nv) {
      $scope.selectionMateriales2 = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Analisis elementales
    $scope.analisisElementales = [
      { name: 'C', selected: false },
      { name: 'H', selected: false },
      { name: 'N', selected: false },
      { name: 'S', selected: false },
      { name: 'Muestra', selected: false },
      { name: 'En cenizas', selected: false },
    ];
    $scope.$watch('analisisElementales | filter:{selected:true}', function (nv) {
      $scope.selectionElementales = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Analisis inmediato
    $scope.analisisInmediato = [
      { name: 'Humedad', selected: false },
      { name: 'Cenizas', selected: false },
      { name: 'Volátiles', selected: false }
    ];
    $scope.$watch('analisisInmediato | filter:{selected:true}', function (nv) {
      $scope.selectionInmediato = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Poder calorifico
    $scope.poderCalorifico = [
      { name: 'Superior', selected: false },
      { name: 'Inferior', selected: false }
    ];
    $scope.$watch('poderCalorifico | filter:{selected:true}', function (nv) {
      $scope.selectionCalorifico = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Formas de carbono
    $scope.formasCarbono = [
      { name: 'TOC (carbono orgánico total)', selected: false },
      { name: 'NPOC (carbono inorgánico no purgable)', selected: false },
      { name: 'TC (carbono total)', selected: false },
      { name: 'Materia orgánica biodegradable', selected: false }
    ];
    $scope.$watch('formasCarbono | filter:{selected:true}', function (nv) {
      $scope.selectionCarbono = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Formas de carbono
    $scope.nitrogenoOrganico = [
      { name: 'Nitrógeno orgánico total (NKT)', selected: false },
      { name: 'NH4 en muestras', selected: false }
    ];
    $scope.$watch('nitrogenoOrganico | filter:{selected:true}', function (nv) {
      $scope.selectionOrganico = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Analisis colorimetria
    $scope.analisisColorimetria = [
      { name: 'Nitritos', selected: false },
      { name: 'Nitratos', selected: false },
      { name: 'Sulfatos', selected: false }
    ];
    $scope.$watch('analisisColorimetria | filter:{selected:true}', function (nv) {
      $scope.selectionColorimetria = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Analisis potenciometria
    $scope.analisisPotenciometria = [
      { name: 'pH', selected: false },
      { name: 'Oxígeno disuelto', selected: false },
      { name: 'Conductividad', selected: false },
      { name: 'NH4', selected: false },
      { name: 'Potencial redox', selected: false }
    ];
    $scope.$watch('analisisPotenciometria | filter:{selected:true}', function (nv) {
      $scope.selectionPotenciometria = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Dureza - dbo - dqo
    $scope.dureza_dbo_dqo = [
      { name: 'Dureza del agua (ºF)', selected: false },
      { name: 'DBO5', selected: false },
      { name: 'DQO', selected: false }
    ];
    $scope.$watch('dureza_dbo_dqo | filter:{selected:true}', function (nv) {
      $scope.selectionDureza_dbo_dqo = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //Cromatografia ionica
    $scope.cromatografiaIonica = [
      { name: 'Floruros', selected: false },
      { name: 'Cloruros', selected: false },
      { name: 'Nitritos', selected: false },
      { name: 'Nitratos', selected: false },
      { name: 'Fosfatos', selected: false },
      { name: 'Sulfatos', selected: false },
      { name: 'Otros', selected: false }
    ];
    $scope.$watch('cromatografiaIonica | filter:{selected:true}', function (nv) {
      $scope.selectionCromatografia = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //BMP - AME - TOX
    $scope.bmpAmeTox = [
      { name: 'BMP', selected: false },
      { name: 'AME', selected: false },
      { name: 'TOX', selected: false }
    ];
    $scope.$watch('bmpAmeTox | filter:{selected:true}', function (nv) {
      $scope.selectionBmpAmeTox = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //ICP
    $scope.icp = [
      { name: 'Al', selected: false },
      { name: 'Ca', selected: false },
      { name: 'Fe', selected: false },
      { name: 'K', selected: false },
      { name: 'Mg', selected: false },
      { name: 'Na', selected: false },
      { name: 'Si', selected: false },
      { name: 'Ti', selected: false },
      { name: 'Li', selected: false },
      { name: 'Sr', selected: false },
      { name: 'Ba', selected: false },
      { name: 'V', selected: false },
      { name: 'Cr', selected: false },
      { name: 'Mo', selected: false },
      { name: 'Mn', selected: false },
      { name: 'Co', selected: false },
      { name: 'Ni', selected: false },
      { name: 'Cu', selected: false },
      { name: 'Ag', selected: false },
      { name: 'Zn', selected: false },
      { name: 'Cd', selected: false },
      { name: 'Hg', selected: false },
      { name: 'Ga', selected: false },
      { name: 'In', selected: false },
      { name: 'Ti', selected: false },
      { name: 'Sn', selected: false },
      { name: 'Pb', selected: false },
      { name: 'P', selected: false },
      { name: 'As', selected: false },
      { name: 'Sb', selected: false },
      { name: 'Bi', selected: false },
      { name: 'Se', selected: false }
    ];
    $scope.$watch('icp | filter:{selected:true}', function (nv) {
      $scope.selectionIcp = nv.map(function (elem) {
        return elem.name;
      });
    }, true);

    //alerts
    $scope.mostrarSuccess = false;
    $scope.mostrarInfo = false;
    $scope.mostrarDanger = false;

    $scope.enviarSolicitud = function() {
      $scope.mostrarSuccess = false;
      $scope.mostrarDanger = false;
      $scope.mostrarInfo = true;

      var materialesFinal = [];

      for (var i = 0; i < $scope.selectionMateriales1.length; i++) {
        materialesFinal.push($scope.selectionMateriales1[i])
      }
      for (var i = 0; i < $scope.selectionMateriales2.length; i++) {
        materialesFinal.push($scope.selectionMateriales2[i])
      }

      var datos = {
        '__metadata': {
          'type': 'SP.Data.AnlisisListItem'
        },
        'Fecha_x0020_solicitud': $scope.fechaSolicitud,
        'Interna_x002f_externa': $scope.solicitud,
        'Nombre_x0020_completo': $scope.nombreCompleto,
        'Correo': $scope.correo,
        'organismo_x002f_empresa': $scope.organismo,
        'Tel_x00e9_fono': $scope.telefono,
        'NIF_x002f_CIF': $scope.nif,
        'Direcci_x00f3_n': $scope.direccion,
        /* // A rellenar por el laboratorio //
        'Cantidad_muestra': "",
        'Tipo_muestra': "",
        'Modo_env_x00ed_o_muestra': "",
        'Naturaleza_muestra': "",
        'Origen_muestra': "",
        'Fecha_recogida_muestra': "",
        'Indicaciones_almacenamiento_mues': "",
        'Indicaciones_manipulacion_muestr': "",
        'Observaciones': "", // Fin - A rellenar por el laboratorio // */
        'Fracciones_muestra_frescaId': $scope.mFrescaValor,
        'Fracciones_muestra_secaId': $scope.mSecaValor,
        'Fracciones_cenizasId': $scope.mCenizasValor,
        'Molienda1': $scope.mMolienda1Valor,
        'Molienda2': $scope.mMolienda2Valor,
        'Molienda3': $scope.mMolienda3Valor,
        'Fracciones_carac_macrosId': $scope.fracciones,
        'Tipo_carac_macros': {"__metadata": {"type":"Collection(Edm.String)"}, "results": materialesFinal},
        'An_x00e1_lisis_elemental': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionElementales},
        'Ratio_x0020_C_x002f_N': $scope.ratiocn,
        'An_x00e1_lisis_inmediato': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionInmediato},
        'Poder_calor_x00ed_fico': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionCalorifico},
        'Formas_carbono': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionCarbono},
        'Nitr_x00f3_geno_x0020_org_x00e1_': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionOrganico},
        'An_x00e1_lisis_colorimetr_x00ed_': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionColorimetria},
        'Potenciometr_x00ed_a': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionPotenciometria},
        'Dureza_DBO_DQO': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionDureza_dbo_dqo},
        'Cromatograf_x00ed_a_i_x00f3_nica': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionCromatografia},
        'BMP_x0020__x002d__x0020_AME_x002': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionBmpAmeTox},
        'ICP': {"__metadata": {"type":"Collection(Edm.String)"}, "results": $scope.selectionIcp},
        'Observaciones_total': $scope.observaciones
      };

      reset();

      var resultado = solicitudService.enviarSolicitud(JSON.stringify(datos))
        .then(function(data) {
          $scope.mostrarInfo = false;
          $scope.mostrarSuccess = true;
          reset();
        })
        .catch(function(err) {
          $scope.mostrarInfo = false;
          $scope.mostrarDanger = true;
        });
    }

    function reset(){
      $scope.mFresca = false;
      $scope.mSeca = false;
      $scope.mCenizas = false;
      $scope.mMolienda1 = false;
      $scope.mMolienda2 = false;
      $scope.mMolienda3 = false;

      $scope.mFrescaValor = "";
      $scope.mSecaValor = "";
      $scope.mCenizasValor = "";
      $scope.mMolienda1Valor = "";
      $scope.mMolienda2Valor = "";
      $scope.mMolienda3Valor = "";

      $scope.nombreCompleto = "";
      $scope.direccion = "";
      $scope.organismo = "";
      $scope.telefono = "";
      $scope.nif = "";
      $scope.solicitud = "Interna";
      $scope.correo = "";
      $scope.fracciones = "";
      $scope.observaciones = "";

      angular.forEach($scope.materiales1, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.materiales2, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.analisisElementales, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.analisisInmediato, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.poderCalorifico, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.formasCarbono, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.nitrogenoOrganico, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.analisisColorimetria, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.analisisPotenciometria, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.dureza_dbo_dqo, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.cromatografiaIonica, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.bmpAmeTox, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.icp, function (item) {
        item.selected = false;
      });
      angular.forEach($scope.ratiocn, function (item) {
        item.selected = false;
      });

    }

});
