'use strict';

/**
 * @ngdoc function
 * @name muestrasApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the muestrasApp
 */
angular.module('muestrasApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
