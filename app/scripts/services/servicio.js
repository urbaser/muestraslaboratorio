'use strict';

/**
 * @ngdoc service
 * @name angularApp.cargarCompetidores
 * @description
 * # cargarCompetidores
 * Service in the angularApp.
 */
angular.module('muestrasApp')
  .service('solicitudService', ['$http', '$q',  function ($http, $q) {

    $http({
      url: "http://urbpru1236:18163/sites/Solicitudes%20laboratorio/_api/lists/getbytitle('Solicitud')/items",
      method: 'GET',
      headers: {
        "Accept": "application/json;odata=verbose"
      },
    }).then(function successCallback(response) {
      console.log(response);
    });

    function enviarSolicitud(solicitud){
      var defered = $q.defer();
      var promise = defered.promise;

      $http({
        url: "http://urbpru1236:18163/sites/Solicitudes%20laboratorio/_api/contextinfo",
        method: 'POST',
        headers: {
          "Accept": "application/json;odata=verbose"
        },
      }).then(function successCallback(response) {
        var requestDigest = response.data.d.GetContextWebInformation.FormDigestValue;

        if(requestDigest !== ""){
          $.ajax({
            url: "http://urbpru1236:18163/sites/Solicitudes%20laboratorio/_api/lists/getbytitle('Solicitud')/items",
            type: "POST",
            data: solicitud,
            headers: {
              "accept": "application/json;odata=verbose",
              "content-type": "application/json;odata=verbose",
              "X-RequestDigest": requestDigest
            },
            complete: function (data) {
              defered.resolve();
            },
            error: function (err) {
              defered.reject();
            }
          });
        }
      }, function errorCallback(response) {
         defered.reject();
      });

      return promise;
    }

    return {
      enviarSolicitud: enviarSolicitud
    };

  }]);
